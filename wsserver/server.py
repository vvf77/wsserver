#!/usr/bin/env python
from urllib.parse import parse_qs

import os

from wsserver.utils import get_params_from_url
from wsserver.config import REDIS_URL, PORT
import asyncio
import websockets
import asyncio_redis

import logging

from websockets.exceptions import ConnectionClosed

logger = logging.getLogger(__name__)
redis_url = REDIS_URL

async def dispatcher(ws, path_and_qs):
    # depending on path just add to needed queue and loop forever (ignoring messages)
    if '?' in path_and_qs:
        path, qs = path_and_qs.split('?', 1)
        params = parse_qs(qs)
    else:
        path = path_and_qs
        qs = None
        params = {}

    # TODO: here might be security check: expect token in the params and validate it using data from redis
    logger.info('Connected to "{}"'.format(path))
    subscriber = await Subscriber.append(path, ws)
    while True:
        try:
            some_data = await ws.recv()
        except ConnectionClosed:
            subscriber.state = 'done'
            some_data = None
            break

        if some_data is None:
            break
        logger.warning('Received unexpected message from websocket {}:\n{}'.format(path, some_data))


# there might be another way is listening to pattern channel through psubscribe

class Subscriber:
    redis_connection = None
    subscriber = None
    subscriber_creating_lock = asyncio.Lock()
    is_running = False
    connections = {}
    subscriptions = ['socketio']  # any non empty list should be at the first time

    def __init__(self, channel, ws):
        self.channel = channel
        self.ws = ws
        self.state = 'ready'
        self.subs = None

    async def send(self, msg):
        if not self.ws.open:
            logger.debug('Socket on {} closed'.format(self.channel))
            self.state = 'done'
            self.connections[self.channel].remove(self)
            return
        await self.ws.send(msg.value)

    @classmethod
    async def run_redis_listener(cls):
        if cls.is_running:
            return
        cls.is_running = True
        subs = await cls.get_redis_subscriber()
        while True:
            try:
                msg = await subs.next_published()
            except Exception as e:
                logger.error(e)
                continue

            logger.debug('Send msg to "{}" to {} subscribers'.format(msg.channel, len(cls.connections.get(msg.channel, []))))
            if cls.connections.get(msg.channel):
                for ws_connection in cls.connections.get(msg.channel, []):
                    if ws_connection.state != 'done':
                        await ws_connection.send(msg)
                    cls.connections[msg.channel] = [ws_connection for ws_connection in cls.connections[msg.channel]
                                                    if ws_connection.state != 'done']
            elif msg.channel in cls.connections:
                logger.debug("Nobody listen the channel {}, unsubsribe from it on next connection".format(msg.channel))
                # next append call will remove this channel from subscriptions
                del cls.connections[msg.channel]
                if not cls.connections:
                    cls.connections['socketio'] = []

    @classmethod
    async def append(cls, path, ws):
        subscriber = Subscriber(path, ws)
        redis_subscriber = await cls.get_redis_subscriber()
        if path not in cls.connections:
            cls.connections[path] = []
            await redis_subscriber.unsubscribe(cls.subscriptions)
            cls.subscriptions = list(cls.connections.keys())
            await redis_subscriber.subscribe(cls.subscriptions)
        cls.connections[path].append(subscriber)
        return subscriber

    @classmethod
    async def get_redis_subscriber(cls):
        if not cls.subscriber_creating_lock.locked() and cls.redis_connection and cls.subscriber:
            return cls.subscriber

        if cls.subscriber_creating_lock.locked():
            logger.debug("get_redis_subscriber locked")

        await cls.subscriber_creating_lock
        try:
            params = get_params_from_url(redis_url)
            cls.redis_connection = await asyncio_redis.Connection.create(**params)
            cls.subscriber = await cls.redis_connection.start_subscribe()
            await cls.subscriber.subscribe(cls.subscriptions)
            return cls.subscriber
        finally:
            cls.subscriber_creating_lock.release()


def run_server(host='localhost', port=PORT):
    logger.info('Start websockets server on {}:{}'.format(host, port))

    start_server = websockets.serve(dispatcher, host, port)

    asyncio.get_event_loop().run_until_complete(start_server)
    asyncio.ensure_future(Subscriber.run_redis_listener())
    asyncio.get_event_loop().run_forever()


def main():
    global redis_url
    host = os.environ.get("WSSERVER_HOST") or 'localhost'
    port = os.environ.get("WSSERVER_PORT") or PORT
    redis_url = os.environ.get("REDIS_URL") or REDIS_URL

    run_server(host=host, port=port)


def run_server_logging_debug():
    logger.setLevel(logging.DEBUG)
    logger.addHandler(logging.StreamHandler())
    wslogger = logging.getLogger('websockets.server')
    wslogger.setLevel(logging.INFO)
    wslogger.addHandler(logging.StreamHandler())
    main()


if __name__ == '__main__':
    run_server_logging_debug()
