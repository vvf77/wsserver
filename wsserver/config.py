from os import environ
REDIS_URL = environ.get('REDIS_URL', 'redis://localhost/0')
PORT = int(environ.get("WSSERVER_PORT", "8765"))
