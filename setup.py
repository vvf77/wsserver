from setuptools import setup, find_packages
from os.path import join, dirname

__version__ = "0.0.1"

entry_points = {
    'console_scripts': [
        'wsserver = wsserver.server:main',
        'wsserver-debug = wsserver.server:run_server_logging_debug'
    ]
}

setup(
    name='wsserver',
    version=__version__,
    description='Websocket server - transleate events from redis to WS',
    url='https://bitbucket.org/vvf77/wsserver',
    author='Vladimir Fedorenko',
    author_email='vvf7723@gmail.com',
    packages=find_packages(),
    entry_points=entry_points,
    include_package_data=True,
    long_description=open(join(dirname(__file__), 'readme.txt')).read(),
    scripts=[],
    zip_safe=False,
    install_requires=[
        'websockets==7.0',
        'asyncio_redis==0.15.1'
    ]
)
